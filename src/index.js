import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import ReactGA from 'react-ga';
import blue from '@material-ui/core/colors/blue';
import { Provider } from 'react-redux';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import 'normalize.css/normalize.css';
import AppNew from './AppNew';
import configureStore from './redux/configureStore';

const theme = createMuiTheme({
  palette: {
    primary: blue,
  },
});
const initialState = {};
const store = configureStore(initialState, BrowserRouter);

ReactGA.initialize('UA-80586885-3', { debug: true });

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <MuiThemeProvider theme={theme}>
        <AppNew />
      </MuiThemeProvider>
    </BrowserRouter>
  </Provider>,
  document.getElementById('root')
);
