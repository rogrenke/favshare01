import React, { Fragment } from 'react';
import { Switch, Route } from 'react-router-dom';
import CreateMapView from './components/createMap/CreateMapView';
import MapView from './components/map/MapView';
import PreviewView from './components/preview/PreviewView';
import LoginView from './components/login/LoginView';
import MyMaps from './components/myMaps/MyMapsView';
import RegisterView from './components/register/RegisterView';
import Header from './components/shared/Header';
import withAuthentication from './components/shared/withAuthentication';
import withUserData from './components/shared/withUserData';

import './Base.css';

const App = () => {
  return (
    <Fragment>
      <Route path="/" component={withUserData(Header)} />
      <Switch>
        <Route exact path="/" component={withAuthentication(CreateMapView)} />
        <Route
          exact
          path="/create-map"
          component={withAuthentication(CreateMapView)}
        />
        <Route exact path="/login" component={withUserData(LoginView)} />
        <Route exact path="/register" component={withUserData(RegisterView)} />
        <Route exact path="/map/:id" component={withAuthentication(MapView)} />
        <Route exact path="/preview/:userId/:id" component={PreviewView} />
        <Route exact path="/my-maps" component={withAuthentication(MyMaps)} />
      </Switch>
    </Fragment>
  );
};

export default App;
