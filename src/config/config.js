export const categories = [
  'Cafe',
  'Bar',
  'Restaurant',
  'Fastfood',
  'Shop',
  'Entertainment',
  'Culture',
  'Historic monument',
  'Nature',
  'Viewpoint',
  'Public square',
  'Other',
];

export default categories;
