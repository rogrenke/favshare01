import { createStore, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import rootReducer from './rootReducer';

export default function configureStore(initialState = {}) {
  let middlewares = applyMiddleware(thunk);
  const IS_PRODUCTION = process.env.NODE_ENV === 'production';

  if (!IS_PRODUCTION) {
    const devTools =
      window && window.devToolsExtension ? window.devToolsExtension() : f => f;

    middlewares = compose(middlewares, devTools);
  }

  return createStore(rootReducer, initialState, middlewares);
}
