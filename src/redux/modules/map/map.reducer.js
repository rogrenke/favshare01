// default state
const initialState = {
  creator: '',
  name: '',
  city: {},
  spots: {},
  activeCategories: [],
  selectedSpot: {},
  placeOfInterest: null,
};

// ------------------------------------
// Constants
// ------------------------------------
export const SET_MAP = 'SET_MAP';
export const SET_SPOT = 'SET_SPOT';
export const SET_ACTIVE_CATEGORIES = 'SET_ACTIVE_CATEGORIES';
export const SET_SELECTED_SPOT = 'SET_SELECTED_SPOT';
export const REMOVE_SELECTED_SPOT = 'REMOVE_SELECTED_SPOT';
export const REMOVE_SPOT = 'REMOVE_SPOT';
export const SET_PLACE_OF_INTEREST = 'SET_PLACE_OF_INTEREST';
export const REMOVE_PLACE_OF_INTEREST = 'REMOVE_PLACE_OF_INTEREST';

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [SET_MAP]: (state, action) => ({
    ...state,
    ...action.payload,
  }),
  [SET_SPOT]: (state, action) => ({
    ...state,
    spots: { ...state.spots, [action.payload.id]: action.payload },
  }),
  [SET_ACTIVE_CATEGORIES]: (state, action) => ({
    ...state,
    activeCategories: action.payload,
  }),
  [SET_SELECTED_SPOT]: (state, action) => ({
    ...state,
    selectedSpot: action.payload,
  }),
  [REMOVE_SELECTED_SPOT]: state => ({
    ...state,
    selectedSpot: {},
  }),
  [REMOVE_SPOT]: (state, action) => {
    const filteredSpots = Object.keys(state.spots)
      .filter(id => id !== action.payload)
      .reduce((spots, id) => {
        spots[id] = state.spots[id];
        return spots;
      }, {});

    return {
      ...state,
      spots: filteredSpots,
    };
  },
  [SET_PLACE_OF_INTEREST]: (state, action) => ({
    ...state,
    placeOfInterest: action.payload,
  }),
  [REMOVE_PLACE_OF_INTEREST]: state => ({
    ...state,
    placeOfInterest: null,
  }),
};

// ------------------------------------
// Reducer
// ------------------------------------
export default function userReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];

  // if we have a handler for that action type call it, if not return state;
  return handler ? handler(state, action) : state;
}
