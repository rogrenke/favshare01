import {
  SET_MAP,
  SET_SPOT,
  SET_ACTIVE_CATEGORIES,
  SET_SELECTED_SPOT,
  REMOVE_SELECTED_SPOT,
  REMOVE_SPOT,
  SET_PLACE_OF_INTEREST,
  REMOVE_PLACE_OF_INTEREST,
} from './map.reducer';

// ------------------------------------
// Selectors
// ------------------------------------
export function getMap(state) {
  return state.map;
}

export function getSpots(state) {
  return state.map.spots;
}

export function getActiveCategories(state) {
  return state.map.activeCategories;
}

export function getSelectedSpot(state) {
  return state.map.selectedSpot;
}

export function getPlaceOfInterest(state) {
  return state.map.placeOfInterest;
}
// ------------------------------------
// Actions
// ------------------------------------
/**
 * Sync Actions
 */
export const setMap = payload => ({
  type: SET_MAP,
  payload,
});

export const setSpot = payload => ({
  type: SET_SPOT,
  payload,
});

export const removeSpot = payload => ({
  type: REMOVE_SPOT,
  payload,
});

export const setActiveCategories = payload => ({
  type: SET_ACTIVE_CATEGORIES,
  payload,
});

export const setSelectedSpot = payload => ({
  type: SET_SELECTED_SPOT,
  payload,
});

export const removeSelectedSpot = () => ({
  type: REMOVE_SELECTED_SPOT,
});

export const setPlaceOfInterest = payload => ({
  type: SET_PLACE_OF_INTEREST,
  payload,
});

export const removePlaceOfInterest = () => ({
  type: REMOVE_PLACE_OF_INTEREST,
});
