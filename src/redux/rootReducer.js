import { combineReducers } from 'redux';
import map from './modules/map/map.reducer';

const rootReducer = combineReducers({
  map,
});

export default rootReducer;
