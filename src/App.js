import React, { Component } from "react";
import "./App.css";
import Map from "./components/Map";
import ApiClient from "./services/ApiClient";
import SpotsList from "./components/SpotsList";
import Header from "./components/Header";
import Filtering from "./components/Filtering";
import Form from "./components/Form";
import ReactGA from 'react-ga';

class App extends Component {
  state = { width: window.innerWidth };
  constructor(props) {
    super(props);
    this.onClickListItem = this.onClickListItem.bind(this);
    this.addNewSpotToState = this.addNewSpotToState.bind(this);
    this.handleWindowSizeChange = this.handleWindowSizeChange.bind(this);
    this.pageViewChanged = this.pageViewChanged.bind(this);
    this.onDelete = this.onDelete.bind(this);
    this.fetchSpots = this.fetchSpots.bind(this);


    this.mapId = props.match ? props.match.params.map : null;
    this.previewMode = props.match ? (props.match.params.previewMode ? true : false) : false;
    if (this.previewMode) {
      ReactGA.pageview('/preview');
    }
  }
  async componentDidMount() {
    window.addEventListener('resize', this.handleWindowSizeChange);
    if (this.mapId) {
      this.setState({ loading: true });
      this.fetchSpots();
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleWindowSizeChange);
  }

  fetchSpots() {
    ApiClient.fetchRecommendedSpots(this.mapId, (recommendedSpots, map) => {
      if (recommendedSpots) {
        recommendedSpots.forEach((s, i) => (s.index = i + 1));
      }
      this.setState({ recommendedSpots, map, loading: false });
    });
  }

  handleWindowSizeChange = () => {
    this.setState({ width: window.innerWidth });
  };

  onClickListItem(item) {
    this.setState({ selectedItem: item });
  }
  addNewSpotToState(newSpot, map) {
    const recommendedSpots = this.state.recommendedSpots ? [...this.state.recommendedSpots, newSpot] : [newSpot];
    recommendedSpots.forEach((s, i) => (s.index = i + 1));
    this.setState({ recommendedSpots, map });
  }
  pageViewChanged(page) {
    if (!this.previewMode) {
      ReactGA.pageview(page);
    }
  }
  onDelete(spotId) {
    if (!this.mapId) return;

    if (!window.confirm('Are you sure you wish to delete this spot?')) return;

    ApiClient.removeSpot(spotId, this.mapId, () => {
      this.fetchSpots();
    });
  }

  render() {
    const isMobile = this.state.width <= 500;
    const list = (<SpotsList
      notPreviewMode={!this.previewMode}
      isMobile={isMobile}
      onItemClick={this.onClickListItem}
      recommendedSpots={this.state.filteredSpots}
      selectedItem={this.state.selectedItem}
      onDelete={this.onDelete}
    />);

    return (

      <div className="App">
        <Header shouldShowShare={!this.previewMode && this.state.map} isMobile={isMobile} pageView={this.pageViewChanged} />
        {!this.previewMode &&
          <Form
            isMobile={isMobile}
            onBoundsDefined={(mapBounds) => this.setState({ mapBounds })}
            onSubmit={(newSpot, map) => {
              if (!this.state.map && map) {
                ApiClient.createNewMap(map, newSpot, mapId => {
                  this.mapId = mapId;
                  this.addNewSpotToState(newSpot, map);
                  const url = `/${mapId}`;
                  this.props.history.push(url);
                });
              } else if (this.state.map) {
                ApiClient.addNewSpot(newSpot, this.mapId, () => {
                  this.addNewSpotToState(newSpot, map);
                });
              }
            }}
            map={this.state.map}
            loading={this.state.loading}
            textFiedChangeFocus={(field) => {
              if (field && isMobile) {
                field.input.scrollIntoView()
              }
              this.setState({ textFieldOnFocus: field !== null });
            }}
            pageView={this.pageViewChanged}
          />}
        <div className="Spots-container">
          <Filtering
            isMobile={isMobile}
            recommendedSpots={this.state.recommendedSpots}
            filteredList={filteredSpots =>
              this.setState({ filteredSpots, selectedItem: null })
            }
          />

          {!isMobile && list}
          <Map
            isMobile={isMobile}
            isPreviewMode={this.previewMode}
            mapBounds={this.state.mapBounds}
            onItemClick={this.onClickListItem}
            recommendedSpots={this.state.filteredSpots}
            selectedItem={this.state.selectedItem}
          />
          {(isMobile && !this.state.textFieldOnFocus) && list}
        </div>
        {isMobile &&
          <div name="dumbViewToShowScroll" style={{ height: 200 }} />
        }
      </div>
    );
  }
}

export default App;
