import firebase from 'firebase/app';
import 'firebase/database';
import 'firebase/auth';

class ApiClient {
  config = {
    apiKey: 'AIzaSyC9a3wdffZv2RJKnh9SiWyDxiCv5eapdsQ',
    authDomain: 'favshare-16675.firebaseapp.com',
    databaseURL: 'https://favshare-16675.firebaseio.com',
    projectId: 'favshare-16675',
    storageBucket: 'favshare-16675.appspot.com',
    messagingSenderId: '406050024643',
  };

  firebaseImpl = firebase.initializeApp(this.config);
  firebaseDatabase = firebase.database();
  firebaseAuth = firebase.auth();

  fetchMap(mapId, userId, callback) {
    const query = this.firebaseDatabase.ref(`maps/${userId}/${mapId}`);
    query.on('value', dataSnapshot => {
      if (!dataSnapshot) {
        callback(null);
        return;
      }
      callback({ ...dataSnapshot.val(), key: dataSnapshot.key });
    });
  }

  fetchMaps(userId, callback) {
    const query = this.firebaseDatabase.ref(`maps/${userId}`);
    query.on('value', dataSnapshot => {
      if (!dataSnapshot.val()) {
        callback(null);
        return;
      }
      const mapsArray = Object.keys(dataSnapshot.val()).map(i => {
        return {
          ...dataSnapshot.val()[i],
          mapId: i,
        };
      });
      callback(mapsArray);
    });
  }

  createNewMap(map, userId, callback) {
    const ref = this.firebaseDatabase.ref(`maps/${userId}`).push();
    const id = ref.key;
    ref.set(map);
    callback(id);
  }

  addNewSpot(spot, mapId, userId, callback) {
    const ref = this.firebaseDatabase
      .ref(`maps/${userId}/${mapId}/spots`)
      .push();
    const id = ref.key;
    ref.set(spot);
    callback(id);
  }

  removeSpot(spotId, mapId, userId, callback) {
    this.firebaseDatabase
      .ref(`maps/${userId}/${mapId}/spots/${spotId}`)
      .remove();
    callback();
  }

  userSignInWithGoogle() {
    localStorage.setItem('socialLogin', 'google');
    var provider = new firebase.auth.GoogleAuthProvider();
    this.firebaseAuth.signInWithRedirect(provider);
  }

  userSignInWithFacebook() {
    localStorage.setItem('socialLogin', 'facebook');
    var provider = new firebase.auth.FacebookAuthProvider();
    this.firebaseAuth.signInWithRedirect(provider);
  }

  userSignOut() {
    localStorage.removeItem('socialLogin');
    this.firebaseAuth
      .signOut()
      .then(() => {
        // Sign-out successful.
        console.log('// Sign-out successful.');
      })
      .catch(error => {
        // An error happened.
        console.log('// Sign-out error.', error);
      });
  }
}
export default new ApiClient();
