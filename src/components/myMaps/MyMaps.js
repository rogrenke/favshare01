import React, { PureComponent } from 'react';
import styled from 'styled-components';
import { Typography } from '@material-ui/core';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

const Wrapper = styled.div`
  margin-top: 20px;
`;

const MapWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  margin-bottom: 20px;
  min-height: 70px;
  padding: 10px;
  cursor: pointer;
  box-shadow: 0px 1px 3px 0px rgba(0, 0, 0, 0.2),
    0px 1px 1px 0px rgba(0, 0, 0, 0.14), 0px 2px 1px -1px rgba(0, 0, 0, 0.12);
`;

class MyMaps extends PureComponent {
  static propTypes = {
    maps: PropTypes.array.isRequired,
  };

  state = {};

  handleClick = mapId => {
    this.setState({ mapId });
  };

  render() {
    return (
      <Wrapper>
        {this.state.mapId && <Redirect push to={`/map/${this.state.mapId}`} />}
        {this.props.maps.map(map => (
          <MapWrapper
            key={map.mapId}
            onClick={() => this.handleClick(map.mapId)}
          >
            <Typography variant="title">{map.name}</Typography>
            <Typography variant="subheading">
              {map.city.formatted_address}
            </Typography>
          </MapWrapper>
        ))}
      </Wrapper>
    );
  }
}

export default MyMaps;
