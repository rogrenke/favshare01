import React, { PureComponent } from 'react';
import Button from '@material-ui/core/Button';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Typography, CircularProgress } from '@material-ui/core';
import ReactGA from 'react-ga';
import { Redirect } from 'react-router-dom';
import ApiClient from '../../services/ApiClient';
import MyMaps from './MyMaps';

const Wrapper = styled.div`
  margin: 20px;
  display: flex;
  flex-direction: column;
`;

const ButtonWrapper = styled.div`
  width: 100%;
  margin-top: 10px;
`;

const CircularProgressWrapper = styled.div`
  align-self: center;
`;

class MyMapsView extends PureComponent {
  static propTypes = {
    userData: PropTypes.object.isRequired,
  };

  state = {};

  componentWillMount() {
    const uid = this.props.userData.uid;
    ApiClient.fetchMaps(uid, maps => {
      this.setState({ maps });
    });
  }

  componentDidMount() {
    ReactGA.pageview(this.props.match.url);
  }

  hanldeClick = () => {
    this.props.history.push('/create-map');
  };

  render() {
    return (
      <Wrapper>
        {this.state.maps === null && <Redirect push to="/create-map" />}
        <Typography variant="headline">My Maps</Typography>
        {!this.state.maps && (
          <CircularProgressWrapper>
            <CircularProgress size={50} />
          </CircularProgressWrapper>
        )}
        {this.state.maps && <MyMaps maps={this.state.maps} />}
        <ButtonWrapper>
          <Button
            variant="contained"
            color="primary"
            type="button"
            fullWidth
            onClick={this.hanldeClick}
          >
            Create new Map
          </Button>
        </ButtonWrapper>
      </Wrapper>
    );
  }
}

export default MyMapsView;
