import React, { Component } from "react";
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from "@material-ui/core/Divider";
import Chip from "@material-ui/core/Chip";
import Icon from "@material-ui/core/Icon";
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from "@material-ui/core/IconButton";

const styles = {
  chip: {
    margin: 4
  },
  wrapper: {
    marginLeft: 10,
    display: "flex",
    flexWrap: "wrap"
  }
};

class MobileSpotsList extends Component {
  render() {
    const bgColor = this.props.selectedItem && this.props.selectedItem.sportName === this.props.firstSpot.sportName ? "#04F06A" : "white";
    return (
      <div
        style={{
          position: "absolute",
          bottom: 0,
          backgroundColor: bgColor,
          width: "99.4vw",
          left: 0,
          border: "1px solid #c3c3c3"
        }}
      >
        <ListItem
          style={{ textAlign: "left" }}
          onClick={() => this.props.onItemClick(this.props.firstSpot)}
          primaryText={`${this.props.firstSpot.index} - ${
            this.props.firstSpot.spotName
          }`}
          secondaryTextLines={2}
          secondaryText={
            <p>
              {this.props.firstSpot.spotAddress}
              <br />"{this.props.firstSpot.spotComment}"
            </p>
          }
          rightIconButton={
            (this.props.selectedItem &&
            this.props.selectedItem.spotName === this.props.firstSpot.spotName && this.props.notPreviewMode) ?
            <IconButton onClick={() => this.props.onDelete(this.props.firstSpot)} style={{marginTop: 30}}>
              <DeleteIcon color={"red"} />
            </IconButton>
            : null
          }
        />
        <div style={styles.wrapper}>
          {this.props.firstSpot.spotCategory01 && (
            <Chip style={styles.chip}>
              {" "}
              {this.props.firstSpot.spotCategory01}{" "}
            </Chip>
          )}
        </div>
        {this.props.secondSpot && (
          <div onClick={() => this.props.onItemClick(this.props.secondSpot)}>
            <Divider />
            <p style={{ paddingLeft: 16 }}>
              {this.props.secondSpot.index} - {this.props.secondSpot.spotName}
            </p>
          </div>
        )}
        <Icon
          className="material-icons"
          style={{ position: "absolute", right: 5, top: 5, color: "#00bcd4" }}
          onClick={() =>
            this.props.updateExpandedList(!this.props.isExpandedList)
          }
        >
          expand_less
        </Icon>

        {this.props.isExpandedList && (
          <div>
            <List
              style={{
                border: "1px solid #c3c3c3",
                position: "absolute",
                bottom: 0,
                backgroundColor: "white",
                width: "99vw",
                left: 0,
                height: "60vh",
                overflow: "scroll"
              }}
            >
              {this.props.listItems}
            </List>

            <Icon
              className="material-icons"
              style={{
                position: "absolute",
                right: 5,
                bottom: "58vh",
                color: "#00bcd4"
              }}
              onClick={() =>
                this.props.updateExpandedList(!this.props.isExpandedList)
              }
            >
              expand_more
            </Icon>
          </div>
        )}
      </div>
    );
  }
}

export default MobileSpotsList;
