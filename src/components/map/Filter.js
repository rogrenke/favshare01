import React, { Component } from 'react';
import Drawer from '@material-ui/core/Drawer';
import PropTypes from 'prop-types';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import styled from 'styled-components';

const Wrapper = styled.div`
  padding: 20px;
`;

const ButtonWrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-gap: 20px;
  margin-top: 20px;
`;

class Filter extends Component {
  static propTypes = {
    categoriesOfGivenSpots: PropTypes.object.isRequired,
    openFilter: PropTypes.bool.isRequired,
    handleCloseFilter: PropTypes.func.isRequired,
    setActiveCategories: PropTypes.func.isRequired,
    activeCategories: PropTypes.array.isRequired,
    removeSelectedSpot: PropTypes.func.isRequired,
  };

  componentWillReceiveProps(nextProps) {
    nextProps.activeCategories.map(activeCategory =>
      this.setState({ [activeCategory]: true })
    );
  }

  handleChange = name => event => {
    this.setState({ [name]: event.target.checked });
  };

  handleFilterClick = () => {
    const currentActiveCategories = Object.keys(this.state).filter(
      key => this.state[key]
    );
    this.props.setActiveCategories(currentActiveCategories);
    this.props.removeSelectedSpot();
    this.props.handleCloseFilter();
  };

  render() {
    return (
      <Drawer anchor="bottom" open={this.props.openFilter}>
        <Wrapper>
          <FormGroup row>
            {Object.entries(this.props.categoriesOfGivenSpots).map(cat => {
              const [category, count] = [cat[0], cat[1]];
              return (
                <FormControlLabel
                  key={category}
                  control={
                    <Checkbox
                      checked={this.state && this.state[category]}
                      onChange={this.handleChange(category)}
                      value={category}
                      color="primary"
                    />
                  }
                  label={`${category} (${count})`}
                />
              );
            })}
          </FormGroup>
          <ButtonWrapper>
            <Button
              variant="outlined"
              color="primary"
              onClick={this.props.handleCloseFilter}
            >
              Cancel
            </Button>
            <Button
              variant="contained"
              color="primary"
              onClick={this.handleFilterClick}
            >
              Filter
            </Button>
          </ButtonWrapper>
        </Wrapper>
      </Drawer>
    );
  }
}

export default Filter;
