import React, { Component } from 'react';
import Drawer from '@material-ui/core/Drawer';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import LinkIcon from '@material-ui/icons/Link';
import Typography from '@material-ui/core/Typography';
import {
  EmailShareButton,
  LinkedinShareButton,
  FacebookShareButton,
  WhatsappShareButton,
  FacebookIcon,
  WhatsappIcon,
  LinkedinIcon,
  EmailIcon,
} from 'react-share';

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
`;

const IconWrapper = styled.div`
  margin: 5px 3px;
  cursor: pointer;
`;

const LinkIconWrapper = styled.div`
  margin: 5px 3px;
  cursor: pointer;
  width: 64px;
  height: 64px;
  display: flex;
  align-items: center;
  justify-content: center;
  color: #fff;
  background: #ffc107;
  flex-direction: column;
`;

const StyledLinkIcon = styled(LinkIcon)`
  && {
    font-size: 40px;
  }
`;

const StyledLinkIconTypo = styled(Typography)`
  && {
    color: #fff;
    margin-top: -8px;
  }
`;
class Share extends Component {
  static propTypes = {
    openShare: PropTypes.bool.isRequired,
    handleCloseShare: PropTypes.func.isRequired,
    url: PropTypes.string.isRequired,
  };

  render() {
    return (
      <Drawer
        anchor="bottom"
        open={this.props.openShare}
        onClick={this.props.handleCloseShare}
        onKeyDown={this.props.handleCloseShare}
      >
        <Wrapper>
          <EmailShareButton url={this.props.url}>
            <IconWrapper>
              <EmailIcon />
            </IconWrapper>
          </EmailShareButton>
          <LinkedinShareButton url={this.props.url}>
            <IconWrapper>
              <LinkedinIcon />
            </IconWrapper>
          </LinkedinShareButton>
          <FacebookShareButton url={this.props.url}>
            <IconWrapper>
              <FacebookIcon />
            </IconWrapper>
          </FacebookShareButton>
          <WhatsappShareButton url={this.props.url}>
            <IconWrapper>
              <WhatsappIcon />
            </IconWrapper>
          </WhatsappShareButton>
          <CopyToClipboard text={this.props.url}>
            <LinkIconWrapper>
              <StyledLinkIcon />
              <StyledLinkIconTypo>copy link</StyledLinkIconTypo>
            </LinkIconWrapper>
          </CopyToClipboard>
        </Wrapper>
      </Drawer>
    );
  }
}
export default Share;
