import React, { Component } from 'react';
import FilterListIcon from '@material-ui/icons/FilterList';
import FormatListBulletedIcon from '@material-ui/icons/FormatListBulleted';
import ShareIcon from '@material-ui/icons/Share';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import styled from 'styled-components';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import AddSpotDialogButton from '../shared/button/AddSpotDialogButton';

const Wrapper = styled.div`
  width: 100%
  position: fixed;
  bottom: 0;
  background: #fff;
  height: 56px;
  display: flex;
  align-items: center;
  justify-content: space-around;
`;

const styles = {
  buttonLabel: {
    flexDirection: 'column',
  },
  buttonRoot: {
    borderRadius: '0',
    width: 'auto',
    padding: '0 10px',
  },
  textRoot: {
    color: '#0000008a',
  },
};

class BottomNav extends Component {
  static propTypes = {
    handleOpenAddSpotDialog: PropTypes.func.isRequired,
    handleOpenList: PropTypes.func.isRequired,
    handleOpenShare: PropTypes.func.isRequired,
    handleOpenFilter: PropTypes.func.isRequired,
    noSpotIsSet: PropTypes.bool.isRequired,
    classes: PropTypes.object.isRequired,
  };

  render() {
    return (
      <Wrapper>
        <IconButton
          disabled={this.props.noSpotIsSet}
          onClick={this.props.handleOpenFilter}
          color="primary"
          classes={{
            label: this.props.classes.buttonLabel,
            root: this.props.classes.buttonRoot,
          }}
        >
          <FilterListIcon />
          <Typography
            classes={{
              root: this.props.classes.textRoot,
            }}
          >
            Filter
          </Typography>
        </IconButton>
        <IconButton
          disabled={this.props.noSpotIsSet}
          onClick={this.props.handleOpenList}
          color="primary"
          classes={{
            label: this.props.classes.buttonLabel,
            root: this.props.classes.buttonRoot,
          }}
        >
          <FormatListBulletedIcon />
          <Typography
            classes={{
              root: this.props.classes.textRoot,
            }}
          >
            List
          </Typography>
        </IconButton>
        <IconButton
          disabled={this.props.noSpotIsSet}
          onClick={this.props.handleOpenShare}
          color="primary"
          classes={{
            label: this.props.classes.buttonLabel,
            root: this.props.classes.buttonRoot,
          }}
        >
          <ShareIcon />
          <Typography
            classes={{
              root: this.props.classes.textRoot,
            }}
          >
            Share
          </Typography>
        </IconButton>
        <AddSpotDialogButton
          handleOpenAddSpotDialog={this.props.handleOpenAddSpotDialog}
        />
      </Wrapper>
    );
  }
}

export default withStyles(styles)(BottomNav);
