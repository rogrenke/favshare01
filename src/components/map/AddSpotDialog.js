import React, { PureComponent } from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import PropTypes from 'prop-types';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import Button from '@material-ui/core/Button';
import Input from '@material-ui/core/Input';
import { withStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import styled from 'styled-components';
import Select from '@material-ui/core/Select';
import { connect } from 'react-redux';
import {
  removePlaceOfInterest,
  getPlaceOfInterest,
} from '../../redux/modules/map/map';
import StandaloneSearchBox from 'react-google-maps/lib/components/places/StandaloneSearchBox';
import { withGoogleMap, GoogleMap, Marker } from 'react-google-maps';
import ApiClient from '../../services/ApiClient';
import { categories } from '../../config/config';

const GoogleMapContent = withGoogleMap(props => (
  <GoogleMap center={{ ...props.center }} defaultZoom={13}>
    {props.isNewFavoriteSet && <Marker position={{ ...props.center }} />}
  </GoogleMap>
));

const mapStateToProps = state => ({
  place: getPlaceOfInterest(state),
});

const mapDispatchToProps = {
  removePlaceOfInterest,
};

const MapWrapper = styled.div`
  width: calc(100vw - 48px);
  height: 57vh;
  margin-top: 20px;
  @media (min-width: 600px) {
    width: 100%;
    height: 380px;
  }
`;

const styles = {
  rootContent: {
    paddingTop: '10px !important',
  },
};

class AddSpotDialog extends PureComponent {
  static propTypes = {
    openAddSpotDialog: PropTypes.bool.isRequired,
    fullScreen: PropTypes.bool,
    handleCloseAddSpotDialog: PropTypes.func.isRequired,
    map: PropTypes.object.isRequired,
    setSpot: PropTypes.func.isRequired,
    mapId: PropTypes.string.isRequired,
    classes: PropTypes.object.isRequired,
    removeSelectedSpot: PropTypes.func.isRequired,
    place: PropTypes.object,
    removePlaceOfInterest: PropTypes.func.isRequired,
    userId: PropTypes.string,
  };

  static defaultProps = {
    fullScreen: true,
  };

  state = {
    comment: '',
    category: '',
    favorite: '',
    newFavorite: null,
    invalidCityField: false,
    invalidCategoryField: false,
    place: null,
  };

  componentDidUpdate(prevProps) {
    if (!Object.is(this.props.place, prevProps.place)) {
      this.setState({
        newFavorite: this.props.place,
        favorite: this.props.place
          ? `${this.props.place.name}, ${this.props.place.formatted_address}`
          : '',
      });
    }
  }

  handleChange = name => event => {
    this.setState({ [name]: event.target.value });

    if (name === 'category' && event.target.value)
      this.setState({ invalidCategoryField: false });
  };

  handleKeyDown = event => {
    if (event.which === 13) {
      event.preventDefault();
    }
  };

  handleCloseDialog = () => {
    this.setState({
      comment: '',
      category: '',
      favorite: '',
      newFavorite: null,
      invalidCityField: false,
      invalidCategoryField: false,
    });
    this.props.handleCloseAddSpotDialog();
  };

  handleSubmit = event => {
    event.preventDefault();

    if (!this.state.newFavorite || !this.state.category) {
      if (!this.state.newFavorite) this.setState({ invalidCityField: true });
      if (!this.state.category) this.setState({ invalidCategoryField: true });
      return;
    }

    const spot = {
      comment: this.state.comment,
      name: this.state.newFavorite.name,
      location: {
        lat: this.state.newFavorite.geometry.location.lat(),
        lng: this.state.newFavorite.geometry.location.lng(),
      },
      address: this.state.newFavorite.formatted_address,
      category: this.state.category,
    };
    ApiClient.addNewSpot(spot, this.props.mapId, this.props.userId, id => {
      this.props.setSpot({ ...spot, id });
    });
    this.setState({
      comment: '',
      category: '',
      favorite: '',
      newFavorite: undefined,
      invalidCityField: false,
    });
    this.props.removeSelectedSpot();
    this.props.handleCloseAddSpotDialog();
  };

  render() {
    let center = {};
    if (this.state.newFavorite) {
      center = {
        lat: this.state.newFavorite.geometry.location.lat(),
        lng: this.state.newFavorite.geometry.location.lng(),
      };
    } else {
      center = {
        ...this.props.map.city.location,
      };
    }

    return (
      <div>
        <Dialog
          fullScreen={this.props.fullScreen}
          open={this.props.openAddSpotDialog}
          onClose={this.handleClose}
        >
          <DialogContent
            classes={{
              root: this.props.classes.rootContent,
            }}
          >
            <form noValidate autoComplete="off" onSubmit={this.handleSubmit}>
              <StandaloneSearchBox
                ref={r => {
                  this.newSpotSearchBox = r;
                }}
                bounds={
                  this.props.map.city ? this.props.map.city.literal : null
                }
                onPlacesChanged={() => {
                  const place = this.newSpotSearchBox.getPlaces()[0];
                  if (!place) {
                    this.setState({ invalidCityField: true });
                    return;
                  }
                  this.setState({
                    invalidCityField: false,
                    newFavorite: place,
                    favorite: `${place.name}, ${place.formatted_address}`,
                  });
                }}
              >
                <FormControl
                  error={this.state.invalidCityField}
                  fullWidth
                  margin="dense"
                >
                  <InputLabel htmlFor="favorite">Add your favorite</InputLabel>
                  <Input
                    required
                    placeholder="e.g Central Park"
                    id="favorite"
                    name="favorite"
                    onChange={this.handleChange('favorite')}
                    value={this.state.favorite}
                    autoComplete="off"
                    onKeyDown={this.handleKeyDown}
                  />
                  {this.state.invalidCityField && (
                    <FormHelperText>
                      Please select your favorite from the list
                    </FormHelperText>
                  )}
                </FormControl>
              </StandaloneSearchBox>
              <FormControl fullWidth margin="dense">
                <InputLabel htmlFor="comment">Comment</InputLabel>
                <Input
                  multiline
                  placeholder="comment - e.g. Go there in the morning"
                  id="comment"
                  name="comment"
                  value={this.state.comment}
                  onChange={this.handleChange('comment')}
                />
              </FormControl>
              <FormControl
                error={this.state.invalidCategoryField}
                fullWidth
                margin="dense"
              >
                <InputLabel htmlFor="category">Category</InputLabel>
                <Select
                  native
                  value={this.state.category}
                  onChange={this.handleChange('category')}
                  inputProps={{
                    name: 'category',
                    id: 'category',
                  }}
                >
                  <option value="" />
                  {categories.map(category => (
                    <option key={category} value={category}>
                      {category}
                    </option>
                  ))}
                </Select>
                {this.state.invalidCategoryField && (
                  <FormHelperText>
                    Please select a category from the list
                  </FormHelperText>
                )}
              </FormControl>
            </form>
            {this.state.newFavorite && (
              <MapWrapper>
                <GoogleMapContent
                  center={center}
                  isNewFavoriteSet={this.state.newFavorite}
                  containerElement={
                    <div style={{ height: `100%`, width: `100%` }} />
                  }
                  mapElement={<div style={{ height: `100%`, width: `100%` }} />}
                />
              </MapWrapper>
            )}
          </DialogContent>
          <DialogActions>
            <Button
              onClick={this.handleCloseDialog}
              variant="outlined"
              color="primary"
            >
              cancel
            </Button>
            <Button
              onClick={this.handleSubmit}
              variant="contained"
              color="primary"
            >
              submit
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withMobileDialog()(withStyles(styles)(AddSpotDialog)));
