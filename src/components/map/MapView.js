import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import styled from 'styled-components';
import CircularProgress from '@material-ui/core/CircularProgress';
import ReactGA from 'react-ga';
import {
  getMap,
  setSpot,
  setMap,
  setActiveCategories,
  getActiveCategories,
  removeSelectedSpot,
  removePlaceOfInterest,
} from '../../redux/modules/map/map';
import Map from '../shared/Map';
import AddSpotDialog from './AddSpotDialog';
import BottomNav from './BottomNav';
import List from '../shared/List';
import Share from './Share';
import Filter from './Filter';
import ApiClient from '../../services/ApiClient';
import { categories } from '../../config/config';

const mapStateToProps = state => ({
  map: getMap(state),
  activeCategories: getActiveCategories(state),
});

const mapDispatchToProps = {
  setSpot,
  setMap,
  setActiveCategories,
  removeSelectedSpot,
  removePlaceOfInterest,
};

const MapWrapper = styled.div`
  width: 100vw;
  height: calc(100vh - 112px);
  margin-bottom: 56px;
  display: flex;
  justify-content: center;
  @media (min-width: 600px) {
    height: calc(100vh - 120px);
  }
`;

const CircularProgressWrapper = styled.div`
  align-self: center;
`;

class MapView extends PureComponent {
  static propTypes = {
    map: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired,
    setSpot: PropTypes.func.isRequired,
    setActiveCategories: PropTypes.func.isRequired,
    setMap: PropTypes.func.isRequired,
    activeCategories: PropTypes.array.isRequired,
    removeSelectedSpot: PropTypes.func.isRequired,
    removePlaceOfInterest: PropTypes.func.isRequired,
    userData: PropTypes.object.isRequired,
  };

  state = {
    openAddSpotDialog: false,
    openList: false,
    openShare: false,
    openFilter: false,
  };

  componentWillMount() {
    const mapId = this.props.match ? this.props.match.params.id : null;
    ApiClient.fetchMap(mapId, this.props.userData.uid, map => {
      this.props.setMap({ ...map });
    });
    this.props.setActiveCategories(categories);
  }

  componentDidMount() {
    ReactGA.pageview(`${this.props.match.path}/${this.props.match.params.id}`);
  }

  handleOpenAddSpotDialog = () => {
    ReactGA.pageview(
      `${this.props.match.path}/addSpotView/${this.props.match.params.id}`
    );
    this.setState({ openAddSpotDialog: true });
  };

  handleCloseAddSpotDialog = () => {
    this.setState({ openAddSpotDialog: false });
    this.props.removePlaceOfInterest();
    ReactGA.pageview(`${this.props.match.path}/${this.props.match.params.id}`);
  };

  handleOpenList = () => {
    ReactGA.pageview(
      `${this.props.match.path}/listView/${this.props.match.params.id}`
    );
    this.setState({ openList: true });
  };

  handleCloseList = () => {
    this.setState({ openList: false });
    ReactGA.pageview(`${this.props.match.path}/${this.props.match.params.id}`);
  };

  handleOpenShare = () => {
    ReactGA.pageview(
      `${this.props.match.path}/shareView/${this.props.match.params.id}`
    );
    this.setState({ openShare: true });
  };

  handleCloseShare = () => {
    this.setState({ openShare: false });
    ReactGA.pageview(`${this.props.match.path}/${this.props.match.params.id}`);
  };

  handleOpenFilter = () => {
    ReactGA.pageview(
      `${this.props.match.path}/filterView/${this.props.match.params.id}`
    );
    this.setState({ openFilter: true });
  };

  handleCloseFilter = () => {
    this.setState({ openFilter: false });
    ReactGA.pageview(`${this.props.match.path}/${this.props.match.params.id}`);
  };

  render() {
    const spotsArray = Object.keys(this.props.map.spots);
    const spots = spotsArray.map(spot => ({
      ...this.props.map.spots[spot],
      spotId: spot,
    }));

    const categoriesOfGivenSpots = spots.reduce((categoriesSum, spot) => {
      categoriesSum[spot.category] = (categoriesSum[spot.category] || 0) + 1; // eslint-disable-line no-param-reassign
      return categoriesSum;
    }, {});

    const filteredSpots = spots.filter(spot =>
      this.props.activeCategories.includes(spot.category)
    );

    return (
      <Fragment>
        <MapWrapper>
          {this.props.map.name && (
            <Map
              map={this.props.map}
              spots={filteredSpots}
              handleOpenAddSpotDialog={this.handleOpenAddSpotDialog}
            />
          )}
          {!this.props.map.name && (
            <CircularProgressWrapper>
              <CircularProgress size={50} />
            </CircularProgressWrapper>
          )}
        </MapWrapper>
        <BottomNav
          handleOpenAddSpotDialog={this.handleOpenAddSpotDialog}
          handleOpenList={this.handleOpenList}
          handleOpenShare={this.handleOpenShare}
          handleOpenFilter={this.handleOpenFilter}
          noSpotIsSet={spotsArray.length === 0}
        />
        <AddSpotDialog
          mapId={this.props.match ? this.props.match.params.id : null}
          openAddSpotDialog={this.state.openAddSpotDialog}
          handleCloseAddSpotDialog={this.handleCloseAddSpotDialog}
          map={this.props.map}
          setSpot={this.props.setSpot}
          removeSelectedSpot={this.props.removeSelectedSpot}
          userId={this.props.userData.uid}
        />
        <Filter
          removeSelectedSpot={this.props.removeSelectedSpot}
          openFilter={this.state.openFilter}
          categoriesOfGivenSpots={categoriesOfGivenSpots}
          handleCloseFilter={this.handleCloseFilter}
          setActiveCategories={this.props.setActiveCategories}
          activeCategories={this.props.activeCategories}
        />
        <List
          mapId={this.props.match ? this.props.match.params.id : null}
          openList={this.state.openList}
          spots={filteredSpots}
          handleCloseList={this.handleCloseList}
          userId={this.props.userData.uid}
        />
        <Share
          openShare={this.state.openShare}
          handleCloseShare={this.handleCloseShare}
          url={window.location.href.replace(
            '/map/',
            `/preview/${this.props.userData.uid}/`
          )}
        />
      </Fragment>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MapView);
