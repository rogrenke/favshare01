import React, { PureComponent, Fragment } from 'react';
import Button from '@material-ui/core/Button';
import styled from 'styled-components';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import ApiClient from '../../services/ApiClient';

const ButtonWrapper = styled.div`
  width: 100%;
  margin-top: 10px;
`;

class RegisterForm extends PureComponent {
  state = {
    email: '',
    password: '',
    passwordConfirmation: '',
    invalidEmailField: false,
    invalidPasswordField: false,
    invalidPasswordConfirmationField: false,
    invalidPasswordMatch: false,
  };

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
    if (this.state.email) this.setState({ invalidEmailField: false });
    if (this.state.password) this.setState({ invalidPasswordField: false });
  };

  handleSubmit = event => {
    event.preventDefault();
    if (!this.state.email || !this.state.password) {
      !this.state.email && this.setState({ invalidEmailField: true });
      !this.state.password && this.setState({ invalidPasswordField: true });
      return;
    }

    if (this.state.password !== this.state.passwordConfirmation) {
      this.setState({ invalidPasswordMatch: true });
      return;
    } else {
      this.setState({ invalidPasswordMatch: false });
    }

    ApiClient.firebaseAuth
      .createUserWithEmailAndPassword(this.state.email, this.state.password)
      .catch(error => {
        if (error.code === 'auth/weak-password') {
          this.setState({ invalidPasswordField: true });
        }
        if (error.code === 'auth/invalid-email') {
          this.setState({ invalidEmailField: true });
        }
      });
  };

  handleKeyDown = event => {
    if (event.which === 13) {
      event.preventDefault();
    }
  };

  render() {
    return (
      <Fragment>
        <form noValidate autoComplete="off" onSubmit={this.handleSubmit}>
          <FormControl
            error={this.state.invalidEmailField}
            fullWidth
            margin="dense"
          >
            <InputLabel htmlFor="email">Email</InputLabel>
            <Input
              placeholder="Email"
              id="email"
              type="email"
              name="email"
              value={this.state.email}
              onChange={this.handleChange('email')}
              onKeyDown={this.handleKeyDown}
            />
            {this.state.invalidEmailField && (
              <FormHelperText>Please enter a valid Email</FormHelperText>
            )}
          </FormControl>

          <FormControl
            error={this.state.invalidPasswordField}
            fullWidth
            margin="dense"
          >
            <InputLabel htmlFor="password">Password</InputLabel>
            <Input
              placeholder="Password"
              id="password"
              type="password"
              name="password"
              value={this.state.password}
              onChange={this.handleChange('password')}
              onKeyDown={this.handleKeyDown}
            />
            {this.state.invalidPasswordField && (
              <FormHelperText>
                Please enter a password with at least 6 character
              </FormHelperText>
            )}
          </FormControl>

          <FormControl
            error={this.state.invalidPasswordMatch}
            fullWidth
            margin="dense"
          >
            <InputLabel htmlFor="password">Password Confirmation</InputLabel>
            <Input
              placeholder="Password Confirmation"
              id="passwordConfirmation"
              type="password"
              name="passwordConfirmation"
              value={this.state.passwordConfirmation}
              onChange={this.handleChange('passwordConfirmation')}
              onKeyDown={this.handleKeyDown}
            />
            {this.state.invalidPasswordMatch && (
              <FormHelperText>The password does not match</FormHelperText>
            )}
          </FormControl>

          <ButtonWrapper>
            <Button variant="contained" color="primary" type="submit" fullWidth>
              Register
            </Button>
          </ButtonWrapper>
        </form>
      </Fragment>
    );
  }
}

export default RegisterForm;
