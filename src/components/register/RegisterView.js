import React, { PureComponent } from 'react';
import styled from 'styled-components';
import ReactGA from 'react-ga';
import { Link } from 'react-router-dom';
import { Redirect } from 'react-router-dom';
import { Typography } from '@material-ui/core';
import PropTypes from 'prop-types';
import RegisterForm from './RegisterForm';

const Wrapper = styled.div`
  margin: 20px;
`;

const SignUpWrapper = styled.div`
  margin-top: 5px;
`;

class RegisterView extends PureComponent {
  static propTypes = {
    userData: PropTypes.object.isRequired,
  };

  componentDidMount() {
    ReactGA.pageview(this.props.match.url);
  }

  render() {
    return (
      <Wrapper>
        {this.props.userData.uid !== '' && <Redirect push to="/create-map" />}
        <RegisterForm setMap={this.props.setMap} />
        <SignUpWrapper>
          <Typography variant="body2">
            You have an account? <Link to="/login">login</Link> here.
          </Typography>
        </SignUpWrapper>
      </Wrapper>
    );
  }
}

export default RegisterView;
