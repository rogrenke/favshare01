import React, { Component } from "react";
import Paper from "@material-ui/core/Paper";

import { withGoogleMap, GoogleMap, OverlayView } from "react-google-maps";

const styles = {
  info: {
    padding: 20,
    margin: 20,
    textAlign: "center",
    display: "inline-block",
    position: "absolute",
    top: -60,
    left: 0,
    padding: 10,
    zIndex: 1000,
    minHeight: 30,
    minWidth: 30,
    color: "black"
  },
  pinpoint: {
    paddingTop: 10,
    height: 30,
    width: 30,
    margin: 20,
    textAlign: "center",
    display: "inline-block",
    boxShadow: "red 0 0 10px 0px"

  },
  pinpointDefault: {
    backgroundColor: "yellow",
  },
  pinpointSelected: {
    backgroundColor: "#04F06A"
  }
};

const getPixelPositionOffset = (width, height) => ({
  x: -(width / 2),
  y: -(height / 2)
});

const GoogleMapContent = withGoogleMap(props => (
  <GoogleMap center={props.center} defaultZoom={13}>
    {props.recommendedSpots
      ? props.recommendedSpots.map(e => (
          <OverlayView
            position={{ lat: e.spotPosition.lat, lng: e.spotPosition.lng }}
            mapPaneName={OverlayView.OVERLAY_MOUSE_TARGET}
            key={e.spotName}
            getPixelPositionOffset={getPixelPositionOffset}
          >
            <Paper
              onClick={() => props.onItemClick(e)}
              style={{...styles.pinpoint, ...(props.selectedItem && props.selectedItem.spotName === e.spotName ? styles.pinpointSelected : styles.pinpointDefault) }}
            >
              {e.index}
            </Paper>
          </OverlayView>
        ))
      : null}
  </GoogleMap>
));

class Map extends Component {
  render() {
    const recommendedSpots = this.props.recommendedSpots;
    const selectedItem = this.props.selectedItem;
    const center = selectedItem
      ? selectedItem.spotPosition
      : recommendedSpots && recommendedSpots.length > 0
        ? recommendedSpots[0].spotPosition
        : this.props.mapBounds;

    return (
      <div
        id="map"
        style={{
          float: "right",
          width: this.props.isMobile ? "100vw" : "calc(50vw - ((100vw - 100%)/2))",
          height: this.props.isMobile ? "60vh" : "70vh",
          maxHeight: "100%",
          backgroundColor: "#cf3f3f3"
        }}
      >
        {((recommendedSpots &&
          recommendedSpots.length > 0) || this.props.mapBounds) && (
            <GoogleMapContent
              center={center}
              recommendedSpots={recommendedSpots}
              selectedItem={selectedItem}
              onItemClick={this.props.onItemClick}
              containerElement={
                <div style={{ height: `100%`, width: "100%" }} />
              }
              mapElement={<div style={{ height: `100%`, width: "100%" }} />}
            />
          )}
      </div>
    );
  }
}
export default Map;
