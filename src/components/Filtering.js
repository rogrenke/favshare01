import React, { Component } from "react";
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

const allValue = "All";

class Filtering extends Component {
  state = {};
  filter = "All";
  constructor() {
    super();
    this.filterBy = this.filterBy.bind(this);
    this.renderTabs = this.renderTabs.bind(this);
    this.individualTabsFor = this.individualTabsFor.bind(this);
  }

  filterBy(filter, recommendedSpots) {
    this.filter = filter;
    if (!recommendedSpots) {
      return;
    }

    const filtered =
      filter === "All"
        ? recommendedSpots
        : recommendedSpots.filter(s => s.spotCategory01 === filter);
    this.props.filteredList(filtered);
  }

  componentWillUpdate(nextProps) {
    if (this.props.recommendedSpots !== nextProps.recommendedSpots) {
      this.filterBy(this.filter, nextProps.recommendedSpots);
    }
  }

  renderTabs(categoriesMap) {
    const categories = Object.keys(categoriesMap);
    return (
      <Tabs
        value={this.filter}
        onChange={f => {
          if (f === "more") return;
          this.filterBy(f, this.props.recommendedSpots);
        }}
        indicatorColor="primary"
        textColor="primary"
        scrollable
        scrollButtons="auto"
      >
        {this.individualTabsFor(categories)}
      </Tabs>
    );
  }

  individualTabsFor(categories) {
    if (categories.length > 3) {
      const secondTab = this.filter !== allValue ? this.filter : categories[1];

      let moreItems = categories.filter(c => c !== allValue && c !== secondTab);
      moreItems.splice(0, 0, "More");

      const tabs = [
        <Tab key={categories[0]} label={categories[0]} value={categories[0]} />,
        <Tab
          key={secondTab}
          label={secondTab}
          value={secondTab}
          style={{ overflow: "hidden" }}
        />,

        <Tab
          key={"more"}
          label={
            <Select
              labelStyle={{ justifyContent: "center", color: "ffffffb3" }}
              style={{ paddingBottom: 8 }}
              underlineStyle={{ border: 0 }}
              value={"More"}
              onChange={(e, i, v) => {
                if (v === "More") return;
                this.filterBy(v, this.props.recommendedSpots);
              }}
            >
              {moreItems.map(c => (
                <MenuItem value={c} key={c}>{c}</MenuItem>
              ))}
            </Select>
          }
          value={"more"}
        />
      ];

      return tabs;
    } else {
      return categories.map(k => <Tab key={k} label={k} value={k} />);
    }
  }

  render() {
    if (!this.props.recommendedSpots) return false;

    let categories = [];
    categories[allValue] = allValue;
    this.props.recommendedSpots.forEach(
      s => (categories[s.spotCategory01] = s.spotCategory01)
    );

    return this.renderTabs(categories);
  }
}
export default Filtering;
