import React, { PureComponent, Fragment } from 'react';
import Button from '@material-ui/core/Button';
import styled from 'styled-components';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import { Typography } from '@material-ui/core';
import FormControl from '@material-ui/core/FormControl';
import ApiClient from '../../services/ApiClient';
import Google from '../../asset/image/google';
import facebook from '../../asset/image/facebook.png';

const ButtonWrapper = styled.div`
  width: 100%;
  margin-top: 10px;
`;

const SocialButton = styled.div`
  cursor: pointer;
  height: 39px;
  line-height: 39px;
  font-weight: bold;
  box-shadow: rgba(0, 0, 0, 0.2) 1px 1px 5px 0px;
  background: rgb(255, 255, 255);
  border-radius: 3px;
  color: #5f6368;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 20px;
`;

const ButtonTextWrapper = styled.div`
  margin-left: 10px;
  color: rgba(0, 0, 0, 0.54);
`;

const Divider = styled.div`
  margin-top: 30px;
  color: rgba(0, 0, 0, 0.54);
`;

class LoginForm extends PureComponent {
  state = {
    email: '',
    password: '',
    invalidLogin: false,
  };

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
    this.setState({ invalidLogin: false });
  };

  handleSubmit = event => {
    event.preventDefault();

    ApiClient.firebaseAuth
      .signInWithEmailAndPassword(this.state.email, this.state.password)
      .catch(error => {
        this.setState({ invalidLogin: true });
      });
  };

  handleGoogleLoginClick = () => {
    ApiClient.userSignInWithGoogle();
  };

  handleFacebookLoginClick = () => {
    ApiClient.userSignInWithFacebook();
  };

  handleKeyDown = event => {
    if (event.which === 13) {
      event.preventDefault();
    }
  };

  render() {
    return (
      <Fragment>
        <SocialButton onClick={this.handleGoogleLoginClick}>
          <Google />
          <ButtonTextWrapper>
            <Typography variant="subheading" color="inherit">
              Log in with google
            </Typography>
          </ButtonTextWrapper>
        </SocialButton>
        <SocialButton onClick={this.handleFacebookLoginClick}>
          <img height="22" src={facebook} alt="facebook" />
          <ButtonTextWrapper>
            <Typography variant="subheading" color="inherit">
              Log in with Facebook
            </Typography>
          </ButtonTextWrapper>
        </SocialButton>
        <Divider>
          <Typography variant="subheading" color="inherit">
            OR
          </Typography>
        </Divider>
        <form noValidate autoComplete="off" onSubmit={this.handleSubmit}>
          <FormControl error={this.state.invalidLogin} fullWidth margin="dense">
            <InputLabel htmlFor="email">Email</InputLabel>
            <Input
              placeholder="Email"
              id="email"
              type="email"
              name="email"
              value={this.state.email}
              onChange={this.handleChange('email')}
              onKeyDown={this.handleKeyDown}
            />
            {this.state.invalidLogin && (
              <FormHelperText>Please enter valid Email</FormHelperText>
            )}
          </FormControl>

          <FormControl error={this.state.invalidLogin} fullWidth margin="dense">
            <InputLabel htmlFor="password">Password</InputLabel>
            <Input
              placeholder="Password"
              id="password"
              type="password"
              name="password"
              value={this.state.password}
              onChange={this.handleChange('password')}
              onKeyDown={this.handleKeyDown}
            />
            {this.state.invalidLogin && (
              <FormHelperText>Please enter a valid password</FormHelperText>
            )}
          </FormControl>

          <ButtonWrapper>
            <Button variant="contained" color="primary" type="submit" fullWidth>
              Login
            </Button>
          </ButtonWrapper>
        </form>
      </Fragment>
    );
  }
}

export default LoginForm;
