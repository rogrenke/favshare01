import React, { PureComponent } from 'react';
import styled from 'styled-components';
import ReactGA from 'react-ga';
import { Link } from 'react-router-dom';
import { Redirect } from 'react-router-dom';
import { Typography, CircularProgress } from '@material-ui/core';
import PropTypes from 'prop-types';
import LoginForm from './LoginForm';

const Wrapper = styled.div`
  margin: 40px 20px 20px;
  text-align: center;
`;

const SignUpWrapper = styled.div`
  margin-top: 5px;
`;

class LoginView extends PureComponent {
  static propTypes = {
    userData: PropTypes.object.isRequired,
  };

  state = {
    validLogin: false,
    redirectFromSocialLogin: false,
  };

  componentDidMount() {
    if (localStorage.getItem('socialLogin')) {
      this.setState({ redirectFromSocialLogin: true });
    }
    ReactGA.pageview(this.props.match.url);
  }

  render() {
    return (
      <Wrapper>
        {this.props.userData.uid !== '' && <Redirect push to="/my-maps" />}
        {this.state.redirectFromSocialLogin && <CircularProgress size={50} />}
        {!this.state.redirectFromSocialLogin && (
          <React.Fragment>
            <LoginForm />
            <SignUpWrapper>
              <Typography variant="body2">
                Don't have an account? <Link to="/register">sign up</Link> here.
              </Typography>
            </SignUpWrapper>
          </React.Fragment>
        )}
      </Wrapper>
    );
  }
}

export default LoginView;
