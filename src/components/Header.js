import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import Snackbar from '@material-ui/core/Snackbar';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import FavShare from '../asset/image/favShare.png';
import '../App.css';

const Logo = styled.div`
  background: url(${FavShare}) no-repeat;
  background-size: contain;
  width: 121px;
  height: 29px;
  cursor: pointer;
`;

class Header extends Component {
  state = {};
  render() {
    return (
      <div>
        <AppBar position="static">
          <Toolbar>
            <Link to="/">
              <Logo />
            </Link>
            {this.props.shouldShowShare && (
              <Button onClick={() => this.setState({ showSharingLink: true })}>
                Share
              </Button>
            )}
          </Toolbar>
        </AppBar>

        {this.state.showSharingLink && (
          <Dialog
            title="Share your map"
            actions={[
              <Button
                color="primary"
                onClick={() => this.setState({ showSharingLink: false })}
              >
                Cancel
              </Button>,
              <CopyToClipboard
                text={
                  typeof window !== 'undefined'
                    ? `${window.location.toString()}/previewMode` //TODO: use another approach to avoid error when user changing url manually
                    : ''
                }
                onCopy={() => {
                  this.props.pageView('step4');
                  this.setState({ showSharingLink: false, showSnack: true });
                }}
              >
                <Button primary={true}>Copy link</Button>
              </CopyToClipboard>,
            ]}
            modal={false}
            open
            onRequestClose={this.handleClose}
          />
        )}
        <Snackbar
          open={this.state.showSnack ? true : false}
          message="Link copied!"
          autoHideDuration={4000}
          onRequestClose={() => this.setState({ showSnack: false })}
        />
      </div>
    );
  }
}

export default Header;
