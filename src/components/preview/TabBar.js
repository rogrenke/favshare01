import React, { PureComponent } from 'react';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import PropTypes from 'prop-types';
import TripOriginIcon from '@material-ui/icons/TripOrigin';
import LocalCafeIcon from '@material-ui/icons/LocalCafe';
import LocalBarIcon from '@material-ui/icons/LocalBar';
import RestaurantIcon from '@material-ui/icons/Restaurant';
import FastfoodIcon from '@material-ui/icons/Fastfood';
import LocalMallIcon from '@material-ui/icons/LocalMall';
import LocalMoviesIcon from '@material-ui/icons/LocalMovies';
import MusicNoteIcon from '@material-ui/icons/MusicNote';
import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
import LocalFloristIcon from '@material-ui/icons/LocalFlorist';
import VisibilityIcon from '@material-ui/icons/Visibility';
import TextureIcon from '@material-ui/icons/Texture';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import { withStyles } from '@material-ui/core/styles';
import { categories } from '../../config/config';

const styles = {
  tabWrapper: {
    flexDirection: 'column-reverse',
  },
};

class TabBar extends PureComponent {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    categoriesOfGivenSpots: PropTypes.object.isRequired,
    setActiveCategories: PropTypes.func.isRequired,
    removeSelectedSpot: PropTypes.func.isRequired,
  };

  state = {
    value: 'All',
  };

  handleChange = (event, value) => {
    this.props.removeSelectedSpot();
    this.setState({ value });
    if (value === 'All') {
      this.props.setActiveCategories(categories);
    } else {
      this.props.setActiveCategories([value]);
    }
  };

  icons = {
    Cafe: LocalCafeIcon,
    Bar: LocalBarIcon,
    Restaurant: RestaurantIcon,
    Fastfood: FastfoodIcon,
    Shop: LocalMallIcon,
    Entertainment: LocalMoviesIcon,
    Culture: MusicNoteIcon,
    'Historic monument': AccountBalanceIcon,
    Nature: LocalFloristIcon,
    Viewpoint: VisibilityIcon,
    'Public square': TextureIcon,
    Other: MoreHorizIcon,
  };

  render() {
    return (
      <Tabs
        value={this.state.value}
        onChange={this.handleChange}
        scrollable
        scrollButtons="off"
      >
        <Tab
          classes={{
            wrapper: this.props.classes.tabWrapper,
          }}
          label="All"
          value="All"
          icon={<TripOriginIcon color="primary" />}
        />
        {Object.entries(this.props.categoriesOfGivenSpots).map(cat => {
          const category = cat[0];
          const IconName = this.icons[category];
          return (
            <Tab
              key={category}
              classes={{
                wrapper: this.props.classes.tabWrapper,
              }}
              value={category}
              label={category}
              icon={<IconName color="primary" />}
            />
          );
        })}
      </Tabs>
    );
  }
}

export default withStyles(styles)(TabBar);
