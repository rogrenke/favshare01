import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import styled from 'styled-components';
import CircularProgress from '@material-ui/core/CircularProgress';
import ReactGA from 'react-ga';
import {
  getMap,
  setMap,
  setActiveCategories,
  getActiveCategories,
  removeSelectedSpot,
} from '../../redux/modules/map/map';
import Map from '../shared/Map';
import ApiClient from '../../services/ApiClient';
import { categories } from '../../config/config';
import TabBar from './TabBar';
import BottomNav from './BottomNav';
import List from '../shared/List';

const mapStateToProps = state => ({
  map: getMap(state),
  activeCategories: getActiveCategories(state),
});

const mapDispatchToProps = {
  setMap,
  setActiveCategories,
  removeSelectedSpot,
};

const MapWrapper = styled.div`
  width: 100vw;
  height: calc(100vh - 128px);
  display: flex;
  justify-content: center;
  @media (min-width: 600px) {
    height: calc(100vh - 136px);
  }
`;

const CircularProgressWrapper = styled.div`
  align-self: center;
`;

class PreviewView extends PureComponent {
  static propTypes = {
    map: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired,
    setActiveCategories: PropTypes.func.isRequired,
    setMap: PropTypes.func.isRequired,
    activeCategories: PropTypes.array.isRequired,
    removeSelectedSpot: PropTypes.func.isRequired,
  };

  state = {
    openList: false,
  };

  componentWillMount() {
    if (!this.props.map.name) {
      const mapId = this.props.match ? this.props.match.params.id : null;
      const userId = this.props.match ? this.props.match.params.userId : null;
      ApiClient.fetchMap(mapId, userId, map => {
        this.props.setMap({ ...map });
      });
    }
    this.props.setActiveCategories(categories);
  }

  componentDidMount() {
    ReactGA.pageview(`${this.props.match.path}/${this.props.match.params.id}`);
  }

  handleOpenList = () => {
    ReactGA.pageview(
      `${this.props.match.path}/listView/${this.props.match.params.id}`
    );
    this.setState({ openList: true });
  };

  handleCloseList = () => {
    this.setState({ openList: false });

    ReactGA.pageview(`${this.props.match.path}/${this.props.match.params.id}`);
  };

  render() {
    const spotsArray = Object.keys(this.props.map.spots);
    const spots = spotsArray.map(spot => this.props.map.spots[spot]);

    const categoriesOfGivenSpots = spots.reduce((categoriesSum, spot) => {
      categoriesSum[spot.category] = (categoriesSum[spot.category] || 0) + 1; // eslint-disable-line no-param-reassign
      return categoriesSum;
    }, {});

    const filteredSpots = spots.filter(spot =>
      this.props.activeCategories.includes(spot.category)
    );

    return (
      <Fragment>
        <TabBar
          categoriesOfGivenSpots={categoriesOfGivenSpots}
          setActiveCategories={this.props.setActiveCategories}
          removeSelectedSpot={this.props.removeSelectedSpot}
        />
        <MapWrapper>
          {this.props.map.name && (
            <Map map={this.props.map} spots={filteredSpots} />
          )}
          {!this.props.map.name && (
            <CircularProgressWrapper>
              <CircularProgress size={50} />
            </CircularProgressWrapper>
          )}
        </MapWrapper>
        <List
          isPreviewMode
          openList={this.state.openList}
          spots={filteredSpots}
          handleCloseList={this.handleCloseList}
        />
        <BottomNav handleOpenList={this.handleOpenList} />
      </Fragment>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PreviewView);
