import React, { Component } from 'react';
import FormatListBulletedIcon from '@material-ui/icons/FormatListBulleted';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import styled from 'styled-components';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';

const Wrapper = styled.div`
  width: 100%
  position: fixed;
  bottom: 0;
  height: 56px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-bottom: 5px;
`;

const styles = {
  buttonLabel: {
    flexDirection: 'column',
  },
  buttonRoot: {
    '&:hover': {
      backgroundColor: '#fff',
    },
    width: '55px',
    height: '55px',
    boxShadow:
      '0px 1px 5px 0px rgba(0, 0, 0, 0.2), 0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 3px 1px -2px rgba(0, 0, 0, 0.12)',
    background: '#fff',
  },
  textRoot: {
    color: '#0000008a',
  },
};

class BottomNav extends Component {
  static propTypes = {
    handleOpenList: PropTypes.func.isRequired,
    classes: PropTypes.object.isRequired,
  };

  render() {
    return (
      <Wrapper>
        <IconButton
          onClick={this.props.handleOpenList}
          color="primary"
          classes={{
            label: this.props.classes.buttonLabel,
            root: this.props.classes.buttonRoot,
          }}
        >
          <FormatListBulletedIcon />
          <Typography
            classes={{
              root: this.props.classes.textRoot,
            }}
          >
            List
          </Typography>
        </IconButton>
      </Wrapper>
    );
  }
}

export default withStyles(styles)(BottomNav);
