import React, { Component } from 'react';
import StandaloneSearchBox from 'react-google-maps/lib/components/places/StandaloneSearchBox';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Select from '@material-ui/core/Select';
import NativeSelect from '@material-ui/core/NativeSelect';
import { withGoogleMap, GoogleMap, OverlayView } from 'react-google-maps';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import StepContent from '@material-ui/core/StepContent';
import StepLabel from '@material-ui/core/StepLabel';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import Snackbar from '@material-ui/core/Snackbar';

const locationForGeometry = geometryLocation => {
  return {
    lat: geometryLocation.lat(),
    lng: geometryLocation.lng(),
  };
};

const GoogleMapContent = withGoogleMap(props => (
  <GoogleMap defaultCenter={props.defaultCenter} defaultZoom={19} />
));

const hardCodedCategories = [
  'cafe',
  'bar',
  'restaurant',
  'fastfood',
  'shop',
  'entertainment',
  'culture',
  'historic monument',
  'nature',
  'viewpoint',
  'public square',
];

class Form extends Component {
  defaultState = {
    newFavorite: null,
    comment: null,
    category: '',
    newFavoriteFieldValue: '',
  };
  state = { ...this.defaultState, activeStep: 0 };

  constructor() {
    super();

    this.submit = this.submit.bind(this);
    this.createMap = this.createMap.bind(this);
  }

  componentDidMount() {
    this.props.pageView('/step1');
  }

  componentWillUpdate(nextProps) {
    const map = nextProps ? { ...nextProps.map } : null;
    if (!this.props.map && map) {
      if (
        !map ||
        !map.name ||
        !map.bounds ||
        !map.bounds.name ||
        !map.bounds.literal
      ) {
        return;
      }
      this.props.pageView('/step3');
      this.setState({
        activeStep: 2,
        mapName: map.name,
        mapDescription: map.description,
        bounds: map.bounds,
      });
    }
  }

  handleChange = name => event => {
    this.setState({ [name]: event.target.value });
  };

  submit() {
    if (!this.state.category) {
      return;
    }
    const location = locationForGeometry(
      this.state.newFavorite.geometry.location
    );
    const newSpot = {
      spotName: this.state.newFavorite.name,
      spotComment: this.state.comment,
      spotAddress: this.state.newFavorite.formatted_address,
      spotPosition: location,
      spotCategory01: this.state.category,
    };
    let map = {
      name: this.state.mapName,
      bounds: {
        name: this.state.bounds.name,
        literal: this.state.bounds.literal,
      },
    };
    if (this.state.mapDescription) {
      map = { ...map, description: this.state.mapDescription };
    }

    this.setState(this.defaultState);
    this.props.onSubmit(newSpot, map);
  }

  createMap() {
    const isEmpty = str => !(str && str.trim().length);
    if (isEmpty(this.state.mapName)) {
      this.setState({ snackMessage: 'Please enter a title for your map' });
      return;
    }
    this.props.pageView('/step2');
    this.setState({ activeStep: 1 });
  }

  render() {
    if (this.props.loading) {
      return <CircularProgress style={{ margin: 10 }} />;
    }

    return (
      <div>
        <Stepper
          activeStep={this.state.activeStep}
          orientation="vertical"
          style={{ margin: 0, padding: 0 }}
        >
          {/* ********** Step 1 **********  */}
          <Step style={{ margin: 0, padding: 0 }}>
            <StepLabel>
              {this.state.mapName && this.state.activeStep > 0
                ? `${this.state.mapName}${
                this.state.mapDescription
                  ? ` - ${this.state.mapDescription}`
                  : ''
                }`
                : 'Give your map a name and a description'}
            </StepLabel>
            <StepContent>
              <TextField
                id="mapNameField"
                placeholder="title - e.g. New York City Highlights"
                style={{ width: '100%' }}
                onChange={e => this.setState({ mapName: e.target.value })}
              />{' '}
              <br />
              <TextField
                id="mapDescriptionField"
                placeholder="description - e.g. 10 must-dos"
                onChange={e =>
                  this.setState({ mapDescription: e.target.value })
                }
                style={{ width: '100%' }}
              />{' '}
              <br />
              <Button
                variant="contained"
                color="primary"
                onClick={this.createMap}
              >
                Create
              </Button>
            </StepContent>
          </Step>

          {/* ********** Step 2 **********  */}
          <Step>
            <StepLabel>
              {this.state.bounds
                ? this.state.bounds.name
                : 'Select a city, district or region'}
            </StepLabel>
            <StepContent>
              <StandaloneSearchBox
                ref={r => {
                  this.boundsSearchBox = r;
                }}
                onPlacesChanged={p => {
                  const place = this.boundsSearchBox.getPlaces()[0];
                  const literal = {
                    south: locationForGeometry(
                      place.geometry.viewport.getSouthWest()
                    ).lat,
                    west: locationForGeometry(
                      place.geometry.viewport.getSouthWest()
                    ).lng,
                    north: locationForGeometry(
                      place.geometry.viewport.getNorthEast()
                    ).lat,
                    east: locationForGeometry(
                      place.geometry.viewport.getNorthEast()
                    ).lng,
                  };
                  const bounds = { ...place, literal };
                  this.props.pageView('/step3');
                  this.setState({
                    bounds: bounds,
                    activeStep: 2,
                  });
                  this.props.onBoundsDefined(
                    locationForGeometry(place.geometry.location)
                  );
                }}
              >
                <TextField
                  id="boundsField"
                  placeholder="e.g New York City"
                  style={{ width: '100%' }}
                  ref={r => (this.boundsField = r)}
                  onFocus={() =>
                    this.props.textFiedChangeFocus(this.boundsField)
                  }
                  onBlur={() => this.props.textFiedChangeFocus(null)}
                />
              </StandaloneSearchBox>
            </StepContent>
          </Step>

          {/* ********** Step 3 **********  */}
          <Step>
            <StepLabel>Add your favorites</StepLabel>
            <StepContent>
              <StandaloneSearchBox
                ref={r => {
                  this.newSpotSearchBox = r;
                }}
                bounds={this.state.bounds ? this.state.bounds.literal : null} //Sets the region to use for biasing query predictions. Results will only be biased towards this area and not be completely restricted to it.
                onPlacesChanged={p =>
                  this.setState({
                    newFavorite: this.newSpotSearchBox.getPlaces()[0],
                    newFavoriteFieldValue: '',
                  })
                }
              >
                <TextField
                  id="newFavoriteField"
                  value={this.state.newFavoriteFieldValue}
                  onChange={e =>
                    this.setState({ newFavoriteFieldValue: e.target.value })
                  }
                  placeholder="e.g Central Park"
                  style={{ width: '100%' }}
                  ref={r => (this.newFavoriteField = r)}
                  onFocus={() =>
                    this.props.textFiedChangeFocus(this.newFavoriteField)
                  }
                  onBlur={() => this.props.textFiedChangeFocus(null)}
                />
              </StandaloneSearchBox>
            </StepContent>
          </Step>
        </Stepper>

        {/* *********** Confirmation Dialog *********** */}
        <Dialog
          open={this.state.newFavorite !== null}

        >
          <DialogTitle>Add your Favorites</DialogTitle>
          <DialogContent>
            <div style={{ flexDirection: 'row', height: 500 }}>
              <div style={{ float: 'left' }}>
                <div style={{ widht: '100%', marginRight: 50 }}>
                  <p>
                    Name: {this.state.newFavorite && this.state.newFavorite.name}
                  </p>
                  <p>
                    Address:{' '}
                    {this.state.newFavorite &&
                      this.state.newFavorite.formatted_address}
                  </p>
                  <TextField
                    name="comment"
                    placeholder="Comment (e.g. Go there in the morning / Book in advance / etc. Also, use #hastags)"
                    multiline={true}
                    rows={3}
                    rowsMax={6}
                    onChange={this.handleChange('comment')}
                  />
                  <br />
                  <Select
                    native
                    value={this.state.category}
                    onChange={this.handleChange('category')}
                    style={{ marginLeft: -24 }}
                    inputProps={{
                      name: 'category',
                    }}
                  >
                    <option value="">Select a Category</option>
                    {hardCodedCategories.map(c => {
                      const categoryCapitalized =
                        c.charAt(0).toUpperCase() + c.slice(1);
                      return (
                        <option
                          key={categoryCapitalized}
                          value={categoryCapitalized}
                        >
                          {categoryCapitalized}
                        </option>
                      );
                    })}
                  </Select>
                </div>
              </div>

              <div style={{ float: 'left' }}>
                <GoogleMapContent
                  defaultCenter={
                    this.state.newFavorite
                      ? this.state.newFavorite.geometry.location
                      : null
                  }
                  containerElement={
                    <div
                      style={{
                        maxHeight: '100%',
                        height: 350,
                        width: this.props.isMobile ? '60vw' : 350,
                        maxWidth: '100%',
                      }}
                    />
                  }
                  mapElement={<div style={{ height: '100%', width: '100%' }} />}
                />
              </div>
            </div>
          </DialogContent>
          <DialogActions>
            <Button onClick={() => this.setState({ newFavorite: null })} color="primary">
              Cancel
            </Button>
            <Button onClick={this.submit} color="primary">
              Subscribe
            </Button>
          </DialogActions>
        </Dialog>
        {
          this.state.snackMessage && (
            <Snackbar
              open={this.state.snackMessage ? true : false}
              message={this.state.snackMessage}
              autoHideDuration={2000}
              onRequestClose={() => this.setState({ snackMessage: null })}
            />
          )
        }
      </div >
    );
  }
}

export default Form;
