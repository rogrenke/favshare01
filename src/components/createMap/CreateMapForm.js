import React, { PureComponent, Fragment } from 'react';
import Button from '@material-ui/core/Button';
import styled from 'styled-components';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import StandaloneSearchBox from 'react-google-maps/lib/components/places/StandaloneSearchBox';
import ApiClient from '../../services/ApiClient';

const locationForGeometry = geometryLocation => {
  return {
    lat: geometryLocation.lat(),
    lng: geometryLocation.lng(),
  };
};

const ButtonWrapper = styled.div`
  width: 100%;
  margin-top: 10px;
`;

class CreateMapForm extends PureComponent {
  static propTypes = {
    setMap: PropTypes.func.isRequired,
    userId: PropTypes.string.isRequired,
  };

  state = {
    title: '',
    redirectToMap: false,
    invalidCityField: false,
    invalidTitleField: false,
  };

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
    if (this.state.title) this.setState({ invalidTitleField: false });
  };

  handleSubmit = event => {
    event.preventDefault();

    if (!this.state.city || !this.state.title) {
      if (!this.state.city) this.setState({ invalidCityField: true });
      if (!this.state.title) this.setState({ invalidTitleField: true });
      return;
    }
    const map = {
      creator: this.props.userId,
      name: this.state.title,
      city: {
        formatted_address: this.state.city.formatted_address,
        location: {
          lat: this.state.city.geometry.location.lat(),
          lng: this.state.city.geometry.location.lng(),
        },
        literal: this.state.city.literal,
      },
      spots: [],
    };
    this.props.setMap(map);
    ApiClient.createNewMap(map, this.props.userId, mapId => {
      this.setState({ redirectToMap: true, mapId });
    });
  };

  handleKeyDown = event => {
    if (event.which === 13) {
      event.preventDefault();
    }
  };

  render() {
    return (
      <Fragment>
        {this.state.redirectToMap && (
          <Redirect push to={`/map/${this.state.mapId}`} />
        )}

        <form noValidate autoComplete="off" onSubmit={this.handleSubmit}>
          <FormControl
            error={this.state.invalidTitleField}
            fullWidth
            margin="dense"
          >
            <InputLabel htmlFor="title">Title</InputLabel>
            <Input
              placeholder="title - e.g. New York City Highlights"
              id="title"
              name="title"
              value={this.state.title}
              onChange={this.handleChange('title')}
              onKeyDown={this.handleKeyDown}
            />
            {this.state.invalidTitleField && (
              <FormHelperText>Please select a title</FormHelperText>
            )}
          </FormControl>
          <StandaloneSearchBox
            ref={r => {
              this.boundsSearchBox = r;
            }}
            onPlacesChanged={p => {
              const place = this.boundsSearchBox.getPlaces()[0];
              if (!place) {
                this.setState({ invalidCityField: true });
                return;
              }
              this.setState({ invalidCityField: false });
              const literal = {
                south: locationForGeometry(
                  place.geometry.viewport.getSouthWest()
                ).lat,
                west: locationForGeometry(
                  place.geometry.viewport.getSouthWest()
                ).lng,
                north: locationForGeometry(
                  place.geometry.viewport.getNorthEast()
                ).lat,
                east: locationForGeometry(
                  place.geometry.viewport.getNorthEast()
                ).lng,
              };
              const city = { ...place, literal };
              this.setState({
                city,
              });
            }}
          >
            <FormControl
              error={this.state.invalidCityField}
              fullWidth
              margin="dense"
            >
              <InputLabel htmlFor="city">City or Region</InputLabel>
              <Input
                required
                placeholder="e.g New York City"
                id="city"
                name="city"
                autoComplete="off"
                onKeyDown={this.handleKeyDown}
              />
              {this.state.invalidCityField && (
                <FormHelperText>
                  Please select a city from the list
                </FormHelperText>
              )}
            </FormControl>
          </StandaloneSearchBox>

          <ButtonWrapper>
            <Button variant="contained" color="primary" type="submit" fullWidth>
              Start
            </Button>
          </ButtonWrapper>
        </form>
      </Fragment>
    );
  }
}

export default CreateMapForm;
