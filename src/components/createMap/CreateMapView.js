import React, { PureComponent } from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';
import ReactGA from 'react-ga';
import CreateMapFrom from './CreateMapForm';
import { setMap } from '../../redux/modules/map/map';

const mapStateToProps = () => ({});
const mapDispatchToProps = { setMap };

const Wrapper = styled.div`
  margin: 20px;
`;

class CreateMapView extends PureComponent {
  static propTypes = {
    setMap: PropTypes.func.isRequired,
    match: PropTypes.object.isRequired,
    userData: PropTypes.object.isRequired,
  };

  state = {};

  componentDidMount() {
    ReactGA.pageview(this.props.match.url);
  }

  render() {
    return (
      <Wrapper>
        <Typography variant="headline">Welcome to FAVshare</Typography>
        <Typography variant="subheading">
          The easy way to share recommendations on a map. Start by entering a
          title and selecting the area of the map.
        </Typography>
        <CreateMapFrom
          setMap={this.props.setMap}
          userId={this.props.userData.uid}
        />
      </Wrapper>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateMapView);
