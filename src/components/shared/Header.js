import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import ApiClient from '../../services/ApiClient';
import FavShare from '../../asset/image/favShare.png';

const Logo = styled.div`
  background: url(${FavShare}) no-repeat;
  background-size: contain;
  width: 121px;
  height: 29px;
  cursor: pointer;
`;

const StyledButton = styled(Button)`
  && {
    margin-left: auto;
    color: #fff;
    border-color: #fff;
  }
`;

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

class Header extends Component {
  static propTypes = {
    match: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
    userData: PropTypes.object.isRequired,
  };

  state = {
    open: false,
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleLogoutClick = () => {
    ApiClient.userSignOut();
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    const isLoggedIn = this.props.userData && this.props.userData.uid !== '';

    return (
      <div>
        <AppBar position="static">
          <Toolbar>
            <Link to="/my-maps">
              <Logo />
            </Link>
            {isLoggedIn && (
              <StyledButton
                variant="outlined"
                color="default"
                onClick={this.handleLogoutClick}
              >
                Logout
              </StyledButton>
            )}
          </Toolbar>
        </AppBar>
        <Dialog
          open={this.state.open}
          TransitionComponent={Transition}
          keepMounted
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-slide-title"
          aria-describedby="alert-dialog-slide-description"
        >
          <DialogTitle id="alert-dialog-slide-title">Save the Link</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-slide-description">
              Copy or send yourself the following link to be able to access and
              edit your map in the future.
              <br />
              <br />
              {window.location.href}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Cancel
            </Button>
            <CopyToClipboard
              text={window.location.href}
              onCopy={() => this.handleClose()}
            >
              <Button color="primary">Copy to Clipboard</Button>
            </CopyToClipboard>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default Header;
