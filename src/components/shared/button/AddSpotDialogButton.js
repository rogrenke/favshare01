import React, { Component } from 'react';
import IconButton from '@material-ui/core/IconButton';
import AddBoxIcon from '@material-ui/icons/AddBox';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';

const styles = {
  buttonLabel: {
    flexDirection: 'column',
  },
  buttonRoot: {
    borderRadius: '0',
    width: 'auto',
    padding: '0 10px',
  },
  textRoot: {
    color: '#0000008a',
  },
};

class AddSpotDialogButton extends Component {
  static propTypes = {
    handleOpenAddSpotDialog: PropTypes.func.isRequired,
  };

  render() {
    return (
      <IconButton
        onClick={this.props.handleOpenAddSpotDialog}
        color="primary"
        classes={{
          label: this.props.classes.buttonLabel,
          root: this.props.classes.buttonRoot,
        }}
      >
        <AddBoxIcon />
        <Typography
          classes={{
            root: this.props.classes.textRoot,
          }}
        >
          Add Spot
        </Typography>
      </IconButton>
    );
  }
}

export default withStyles(styles)(AddSpotDialogButton);
