import React, { PureComponent } from 'react';
import Drawer from '@material-ui/core/Drawer';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Chip from '@material-ui/core/Chip';
import DeleteIcon from '@material-ui/icons/Delete';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import {
  setSelectedSpot,
  removeSelectedSpot,
  removeSpot,
} from '../../redux/modules/map/map';
import ApiClient from '../../services/ApiClient';

const mapStateToProps = () => ({});

const mapDispatchToProps = {
  setSelectedSpot,
  removeSelectedSpot,
  removeSpot,
};

const ListWrapper = styled.div`
  padding: 20px;
`;

const ListItem = styled.div`
  padding: 10px 0 15px;
`;

const ContentHeader = styled.div`
  display: flex;
`;

const styles = {
  paperAnchorBottom: {
    top: '30vh',
  },
};

const StyledDeleteIcon = styled(DeleteIcon)`
  cursor: pointer;
  margin-left: auto;
`;

class List extends PureComponent {
  static propTypes = {
    spots: PropTypes.array.isRequired,
    classes: PropTypes.object.isRequired,
    openList: PropTypes.bool.isRequired,
    handleCloseList: PropTypes.func.isRequired,
    setSelectedSpot: PropTypes.func.isRequired,
    removeSelectedSpot: PropTypes.func.isRequired,
    removeSpot: PropTypes.func.isRequired,
    mapId: PropTypes.string,
    isPreviewMode: PropTypes.bool,
    userId: PropTypes.string,
  };

  static defaultProps = {
    isPreviewMode: false,
    mapId: '',
  };

  handelSpotClick = spot => {
    this.props.setSelectedSpot(spot);
  };

  handelDeleteClick = (spot, event) => {
    event.stopPropagation();
    this.props.removeSelectedSpot();
    ApiClient.removeSpot(
      spot.spotId,
      this.props.mapId,
      this.props.userId,
      () => {
        this.props.removeSpot(spot.spotId);
      }
    );
  };

  render() {
    return (
      <Drawer
        anchor="bottom"
        open={this.props.openList}
        onClick={this.props.handleCloseList}
        classes={{
          paperAnchorBottom: this.props.classes.paperAnchorBottom,
        }}
      >
        <ListWrapper>
          {this.props.spots.map((spot, index) => (
            <div key={spot.name}>
              <ListItem onClick={() => this.handelSpotClick(spot)}>
                <ContentHeader>
                  <Typography variant="title" gutterBottom>
                    {`${index + 1}. ${spot.name}`}
                  </Typography>
                  {!this.props.isPreviewMode && (
                    <StyledDeleteIcon
                      onClick={event => this.handelDeleteClick(spot, event)}
                    />
                  )}
                </ContentHeader>
                <Typography variant="subheading" gutterBottom>
                  {spot.comment}
                </Typography>
                <Typography variant="body2" gutterBottom>
                  {spot.address}
                </Typography>
                <Chip label={spot.category} />
              </ListItem>
              {this.props.spots.length - 1 !== index && <Divider />}
            </div>
          ))}
        </ListWrapper>
      </Drawer>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(List));
