import React, { Component } from 'react';
import ApiClient from '../../services/ApiClient';

export default WrappedComponent => {
  class WithAuthentication extends Component {
    state = {
      userData: {
        uid: '',
      },
    };

    componentWillMount() {
      ApiClient.firebaseAuth.onAuthStateChanged(user => {
        if (user) {
          this.setState({ userData: { uid: user.uid } });
        } else {
          this.props.history.push('/login');
        }
      });
    }

    render() {
      return (
        this.state.userData.uid !== '' && (
          <WrappedComponent {...this.props} userData={this.state.userData} />
        )
      );
    }
  }

  return WithAuthentication;
};
