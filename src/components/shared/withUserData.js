import React, { Component } from 'react';
import ApiClient from '../../services/ApiClient';

export default WrappedComponent => {
  class WithUserData extends Component {
    state = {
      userData: {
        uid: '',
      },
    };

    componentWillMount() {
      ApiClient.firebaseAuth.onAuthStateChanged(user => {
        if (user) {
          this.setState({ userData: { uid: user.uid } });
        }
      });
    }

    render() {
      return (
        <WrappedComponent {...this.props} userData={this.state.userData} />
      );
    }
  }

  return WithUserData;
};
