import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import {
  withGoogleMap,
  GoogleMap,
  Marker,
  InfoWindow,
} from 'react-google-maps';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { getSelectedSpot } from '../../redux/modules/map/map';
import { MAP } from 'react-google-maps/lib/constants';
import AddSpotDialogButton from './button/AddSpotDialogButton';
import { setPlaceOfInterest } from '../../redux/modules/map/map';

const PlaceInfoContent = styled.div`
  border-bottom: 1px solid #ccc;
`;

const PlaceInfoButtonWrapper = styled.div`
  margin-top: 10px;
`;

const mapStateToProps = state => ({
  selectedSpot: getSelectedSpot(state),
});

const mapDispatchToProps = {
  setPlaceOfInterest,
};

const GoogleMapContent = withGoogleMap(props => {
  let bounds = new window.google.maps.LatLngBounds();
  let coordinates = [];
  let mapInstance = {};

  // show the given spots on the map
  if (props.spots.length > 1) {
    coordinates = props.spots.map(spot => {
      const { lat, lng } = spot.location;
      const latLng = new window.google.maps.LatLng(lat, lng);
      bounds.extend(latLng);
      return latLng;
    });
  }
  // If only on spot is set or no spot is set hack the zoom and region of the map
  else {
    const lat = props.spots[0]
      ? props.spots[0].location.lat
      : props.cityCenter.lat;
    const lng = props.spots[0]
      ? props.spots[0].location.lng
      : props.cityCenter.lng;

    // extend the bounds with the spot or the city coordinates and add fake spot to center the map
    const latLng0 = new window.google.maps.LatLng(lat, lng);
    bounds.extend(latLng0);
    const latLng1 = new window.google.maps.LatLng(lat + 0.007, lng);
    bounds.extend(latLng1);
    const latLng2 = new window.google.maps.LatLng(lat - 0.007, lng);
    bounds.extend(latLng2);
  }

  // if user selected a marker from the list create bounds for the selected marker
  if (props.selectedSpot.name) {
    const { lat, lng } = props.selectedSpot.location;
    bounds = new window.google.maps.LatLngBounds();
    // extend the bounds with the spot or the city coordinates and add fake spot to center the map
    const latLng0 = new window.google.maps.LatLng(lat, lng);
    bounds.extend(latLng0);
    const latLng1 = new window.google.maps.LatLng(lat + 0.003, lng);
    bounds.extend(latLng1);
    const latLng2 = new window.google.maps.LatLng(lat - 0.003, lng);
    bounds.extend(latLng2);
  }

  const handelRef = map => {
    map.fitBounds(bounds);
    mapInstance = map;
  };

  const handelClickOfAddSpotButton = () => {
    props.closePlaceInfoWindow();
    props.setPlaceOfInterest(props.place);
  };

  return (
    <GoogleMap
      defaultOptions={{
        streetViewControl: false,
        mapTypeControl: false,
      }}
      spots={props.spots}
      ref={map => map && handelRef(map)}
      onClick={event => props.onMapClick(event, mapInstance)}
    >
      {props.isPlaceInfoWindowOpen && (
        <InfoWindow
          defaultOptions={{ maxWidth: 320 }}
          defaultPosition={{
            lat: props.place.geometry.location.lat() + 0.0005,
            lng: props.place.geometry.location.lng(),
          }}
          onCloseClick={props.closePlaceInfoWindow}
        >
          <div>
            <PlaceInfoContent>
              <Typography variant="title" gutterBottom>
                {props.place.name}
              </Typography>

              <Typography variant="body2" gutterBottom>
                {props.place.formatted_address}
              </Typography>
            </PlaceInfoContent>
            <PlaceInfoButtonWrapper onClick={handelClickOfAddSpotButton}>
              <AddSpotDialogButton
                handleOpenAddSpotDialog={props.handleOpenAddSpotDialog}
              />
            </PlaceInfoButtonWrapper>
          </div>
        </InfoWindow>
      )}
      {props.spots.length > 1 &&
        coordinates.map(({ lat, lng }, index) => (
          <Marker
            label={`${index + 1}`}
            key={lat()}
            position={{ lat: lat(), lng: lng() }}
            animation={window.google.maps.Animation.DROP}
            onClick={() => props.toggleOpenMarkerId(index)}
          >
            {props.clickedMarkerId === index && (
              <InfoWindow
                defaultOptions={{ maxWidth: 320 }}
                onCloseClick={() => props.toggleOpenMarkerId(null)}
              >
                <div>
                  <Typography variant="title" gutterBottom>
                    {props.spots[index].name}
                  </Typography>

                  <Typography variant="subheading" gutterBottom>
                    {props.spots[index].comment}
                  </Typography>
                  <Typography variant="body2" gutterBottom>
                    {props.spots[index].address}
                  </Typography>
                </div>
              </InfoWindow>
            )}
          </Marker>
        ))}
      {props.spots.length === 1 && (
        <Marker
          label="1"
          animation={window.google.maps.Animation.DROP}
          position={{
            lat: props.spots[0].location.lat,
            lng: props.spots[0].location.lng,
          }}
          onClick={() => props.toggleOpenMarkerId(0)}
        >
          {props.clickedMarkerId === 0 && (
            <InfoWindow
              defaultOptions={{ maxWidth: 320 }}
              onClose={() => props.toggleOpenMarkerId(null)}
              onCloseClick={() => props.toggleOpenMarkerId(null)}
            >
              <div>
                <Typography variant="title" gutterBottom>
                  {props.spots[0].name}
                </Typography>

                <Typography variant="subheading" gutterBottom>
                  {props.spots[0].comment}
                </Typography>
                <Typography variant="body2" gutterBottom>
                  {props.spots[0].address}
                </Typography>
              </div>
            </InfoWindow>
          )}
        </Marker>
      )}
    </GoogleMap>
  );
});

class Map extends Component {
  static propTypes = {
    map: PropTypes.object.isRequired,
    spots: PropTypes.array.isRequired,
    selectedSpot: PropTypes.object.isRequired,
    handleOpenAddSpotDialog: PropTypes.func,
    setPlaceOfInterest: PropTypes.func.isRequired,
  };

  state = {
    clickedMarkerId: null,
    isPlaceInfoWindowOpen: false,
    place: null,
    handleOpenAddSpotDialog: () => {},
  };

  toggleOpenMarkerId = id => {
    this.setState({ clickedMarkerId: id });
    this.state.isPlaceInfoWindowOpen && this.closePlaceInfoWindow();
  };

  closePlaceInfoWindow = () => {
    this.setState({ isPlaceInfoWindowOpen: false });
  };

  onMapClick = (event, map) => {
    this.state.clickedMarkerId !== null &&
      this.setState({ clickedMarkerId: null });
    this.state.isPlaceInfoWindowOpen && this.closePlaceInfoWindow();
    if (event.placeId) {
      const service = new window.google.maps.places.PlacesService(
        map.context[MAP]
      );

      service.getDetails({ placeId: event.placeId }, (place, status) => {
        this.setState({ isPlaceInfoWindowOpen: true, place });
      });
      event.stop();
    }
  };

  render() {
    const cityCenter = {
      ...this.props.map.city.location,
    };
    return (
      <GoogleMapContent
        onMapClick={this.onMapClick}
        isPlaceInfoWindowOpen={this.state.isPlaceInfoWindowOpen}
        place={this.state.place}
        handleOpenAddSpotDialog={this.props.handleOpenAddSpotDialog}
        toggleOpenMarkerId={this.toggleOpenMarkerId}
        closePlaceInfoWindow={this.closePlaceInfoWindow}
        clickedMarkerId={this.state.clickedMarkerId}
        clickedOnPlace={this.state.clickedOnPlace}
        selectedSpot={this.props.selectedSpot}
        setPlaceOfInterest={this.props.setPlaceOfInterest}
        cityCenter={cityCenter}
        spots={this.props.spots}
        containerElement={<div style={{ height: `100%`, width: `100%` }} />}
        mapElement={<div style={{ height: `100%`, width: `100%` }} />}
      />
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Map);
