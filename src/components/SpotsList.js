import React, { Component } from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import Divider from '@material-ui/core/Divider';
import Chip from '@material-ui/core/Chip';
import MobileSpotsList from './MobileSpotsList';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';

const styles = {
  chip: {
    margin: 4,
  },
  wrapper: {
    marginLeft: 10,
    display: 'flex',
    flexWrap: 'wrap',
  },
};

class SpotsList extends Component {
  state = {};
  constructor() {
    super();

    this.onDelete = this.onDelete.bind(this);
  }
  componentDidUpdate() {
    const recommendedSpots = this.props.recommendedSpots;
    const selectedItem = this.props.selectedItem;
    if (recommendedSpots && selectedItem && this.scroll) {
      const selectedIndex = recommendedSpots.findIndex(
        e => e.spotName === selectedItem.spotName
      );
      if (selectedIndex === -1) return;

      const itemHeight = 82;
      this.scroll.scrollTop = selectedIndex * itemHeight;
    }
  }
  onDelete(spot) {
    this.props.onDelete(spot.spotId);
  }
  render() {
    const recommendedSpots = this.props.recommendedSpots;
    const firstSpot = this.props.selectedItem
      ? this.props.selectedItem
      : recommendedSpots && recommendedSpots.length > 0
        ? recommendedSpots[0]
        : null;
    if (!(recommendedSpots && recommendedSpots.length > 0)) return false;

    const listItems = recommendedSpots.map((e, index) => (
      <div
        style={{
          backgroundColor:
            this.props.selectedItem &&
            this.props.selectedItem.spotName === e.spotName
              ? '#04F06A'
              : 'white',
        }}
        key={e.spotName}
      >
        <ListItem
          style={{ textAlign: 'left' }}
          onClick={() => {
            this.props.onItemClick(e);
            this.setState({ isExpandedList: false });
          }}
        >
          <ListItemText
            primary={`${e.index} - ${e.spotName}`}
            secondary={
              <p>
                {e.spotAddress}
                <br />"{e.spotComment}"
              </p>
            }
          />
          <ListItemSecondaryAction>
            {this.props.selectedItem &&
              this.props.selectedItem.spotName === e.spotName &&
              this.props.notPreviewMode && (
                <IconButton
                  aria-label="Delete"
                  onClick={() => this.onDelete(e)}
                >
                  <DeleteIcon color={'red'} />
                </IconButton>
              )}
          </ListItemSecondaryAction>
        </ListItem>
        <div style={styles.wrapper}>
          {e.spotCategory01 && (
            <Chip style={styles.chip} label={e.spotCategory01} />
          )}
        </div>
        {index + 1 < recommendedSpots.length && <Divider />}
      </div>
    ));

    return (
      <div>
        {this.props.isMobile && (
          <MobileSpotsList
            listItems={listItems}
            firstSpot={firstSpot}
            selectedItem={this.props.selectedItem}
            {...this.props}
            isExpandedList={this.state.isExpandedList}
            updateExpandedList={isExpandedList =>
              this.setState({ isExpandedList })
            }
            onDelete={this.onDelete}
            notPreviewMode={this.props.notPreviewMode}
            secondSpot={
              !this.props.selectedItem
                ? recommendedSpots.length > 1
                  ? recommendedSpots[1]
                  : null
                : null
            }
          />
        )}

        {/* ********* not mobile ****************  */}

        {!this.props.isMobile && (
          <div
            style={{
              width: 'calc(50vw - ((100vw - 100%)/2))',
              height: '70vh',
              overflowY: 'scroll',
              float: 'left',
            }}
            ref={ref => (this.scroll = ref)}
          >
            <List style={{ border: '1px solid #c3c3c3', marginLeft: 10 }}>
              {listItems}
            </List>
          </div>
        )}
      </div>
    );
  }
}

export default SpotsList;
