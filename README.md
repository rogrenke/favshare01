# FAVShare

The easy way to share recommendations on a map.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

[Node.js and npm](https://www.taniarascia.com/how-to-install-and-use-node-js-and-npm-mac-and-windows/)
<br />
[Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli)
<br />
Use [https://prettier.io/](https://prettier.io/) in order to push always proper formated code.


### Installing

A step by step series of examples that tell you how to get a development env running

Login to heroku

```
heroku login
```

Clone the repository

```
heroku git:clone -a favshare$
```

Go into the favshare Folder and run

```
npm install
```

Start the app localy with

```
npm start
```

## Deployment

commit your changes and push to bitbucket

```
git push -u origin HEAD
```

push to heroku

```
git push heroku master
```
